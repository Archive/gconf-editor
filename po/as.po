# translation of as.po to Assamese
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Amitakhya Phukan <amitakhya@svn.gnome.org>, 2007.
# Amitakhya Phukan <aphukan@redhat.com>, 2007.
# Amitakhya Phukan <aphukan@fedoraproject.org>, 2009.
# Nilamdyuti Goswami <ngoswami@redhat.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: as\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug."
"cgi?product=gconf-editor&component=general\n"
"POT-Creation-Date: 2010-05-12 22:44+0000\n"
"PO-Revision-Date: 2011-08-02 20:32+0530\n"
"Last-Translator: Nilamdyuti Goswami <ngoswami@redhat.com>\n"
"Language-Team: Assamese <kde-i18n-doc@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n!=1)\n"

#: ../data/gconf-editor.desktop.in.in.h:1 ../src/gconf-editor-window.c:393
#: ../src/gconf-editor-window.c:395 ../src/gconf-editor-window.c:873
#: ../src/gconf-editor-window.c:1372
msgid "Configuration Editor"
msgstr "সংৰূপ সম্পাদক"

#: ../data/gconf-editor.desktop.in.in.h:2
msgid "Directly edit your entire configuration database"
msgstr "আপোনাৰ সম্পূৰ্ণ সংৰূপ ডাটাবেইচ প্ৰত্যক্ষভাৱে সম্পাদন কৰক"

#: ../data/gconf-editor.schemas.in.h:1
msgid "Bookmarks"
msgstr "পত্ৰচিহ্নসমূহ"

#: ../data/gconf-editor.schemas.in.h:2
msgid "gconf-editor folder bookmarks"
msgstr "gconf-সম্পাদক ফোল্ডাৰৰ পত্ৰচিহ্নসমূহ"

#: ../src/gconf-bookmarks-dialog.c:205
msgid "Edit Bookmarks"
msgstr "পত্ৰচিহ্নসমূহ সম্পাদনা কৰক"

#: ../src/gconf-cell-renderer.c:144 ../src/gconf-cell-renderer.c:343
msgid "<no value>"
msgstr "<no value>"

#: ../src/gconf-editor-window.c:359
#, c-format
msgid "Couldn't display help: %s"
msgstr "সহায় প্ৰদৰ্শন কৰিব নোৱাৰি: %s"

#: ../src/gconf-editor-window.c:399
msgid "An editor for the GConf configuration system."
msgstr "GConf সংৰূপ প্ৰণালীৰ বাবে এটা  সম্পাদক।"

#: ../src/gconf-editor-window.c:402
msgid "translator-credits"
msgstr "অমিতাক্ষ ফুকন (aphukan@fedoraproject.org), নীলমদ্যুতি গোস্বামী (ngoswami@redhat.com)"

#: ../src/gconf-editor-window.c:429
#, c-format
msgid ""
"Couldn't unset key. Error was:\n"
"%s"
msgstr ""
"চাবি অসংহতি কৰিব পৰা নগল। ত্ৰুটি আছিল:\n"
"%s"

#: ../src/gconf-editor-window.c:454
#, c-format
msgid ""
"Could not create key. The error is:\n"
"%s"
msgstr ""
"চাবি সৃষ্টি কৰিব পৰা নগল। ত্ৰুটি হ'ল:\n"
"%s"

#: ../src/gconf-editor-window.c:536
#, c-format
msgid ""
"Could not change key value. Error message:\n"
"%s"
msgstr ""
"চাবিৰ মান পৰিবৰ্তন কৰিব পৰা নগল। ত্ৰুটিৰ বাৰ্তা:\n"
"%s"

#: ../src/gconf-editor-window.c:568
msgid ""
"Currently pairs and schemas can't be edited. This will be changed in a later "
"version."
msgstr ""
"বৰ্তমানে যোৰসমূহ আৰু আঁচনিসমূহ সম্পাদন কৰা সম্ভৱ নহয়। এইটো পৰবৰ্তী সংস্কৰণত "
"পৰিবৰ্তন কৰা হ'ব।"

#: ../src/gconf-editor-window.c:648
#, c-format
msgid ""
"Could not set value. Error was:\n"
"%s"
msgstr ""
"মান সংহতি কৰিব পৰা নগল। ত্ৰুটি আছিল:\n"
"%s"

#: ../src/gconf-editor-window.c:684 ../src/gconf-editor-window.c:728
#, c-format
msgid ""
"Could not sync value. Error was:\n"
"%s"
msgstr ""
"মান সংমিহলি কৰিব পৰা নগল। ত্ৰুটি আছিল:\n"
"%s"

#: ../src/gconf-editor-window.c:740
msgid "_File"
msgstr "নথিপত্ৰ (_F)"

#: ../src/gconf-editor-window.c:741
msgid "_Edit"
msgstr "সম্পাদনা কৰক (_E)"

#: ../src/gconf-editor-window.c:742
msgid "_Search"
msgstr "সন্ধান কৰক (_S)"

#: ../src/gconf-editor-window.c:743
msgid "_Bookmarks"
msgstr "পত্ৰচিহ্নসমূহ (_B)"

#: ../src/gconf-editor-window.c:744
msgid "_Help"
msgstr "সহায় (_H)"

#: ../src/gconf-editor-window.c:746
msgid "New _Settings Window"
msgstr "নতুন সংহতিসমূহ উইন্ডো (_S)"

#: ../src/gconf-editor-window.c:747
msgid "Open a new Configuration Editor window editing current settings"
msgstr "বৰ্তমান সংহতিসমূহ সম্পাদন কৰি এটা নতুন সংৰূপ সম্পাদক উইন্ডো খোলক"

#: ../src/gconf-editor-window.c:749
msgid "New _Defaults Window"
msgstr "নতুন অবিকল্পিত উইন্ডো (_D)"

#: ../src/gconf-editor-window.c:750
msgid "Open a new Configuration Editor window editing system default settings"
msgstr ""
"চিস্টেমৰ অবিকল্পিত সংহতিসমূহ সম্পাদন কৰি এটা নতুন সংৰূপ সম্পাদক উইন্ডো খোলক"

#: ../src/gconf-editor-window.c:752
msgid "New _Mandatory Window"
msgstr "নতুন বাধ্যতামূলক উইন্ডো (_M)"

#: ../src/gconf-editor-window.c:753
msgid "Open a new Configuration Editor window editing system mandatory settings"
msgstr ""
"চিস্টেমৰ বাধ্যতামূলক সংহতিসমূহ সম্পাদন কৰি এটা নতুন সংৰূপ সম্পাদক উইন্ডো "
"খোলক"

#: ../src/gconf-editor-window.c:755
msgid "_Close Window"
msgstr "উইন্ডো বন্ধ কৰক (_C)"

#: ../src/gconf-editor-window.c:755
msgid "Close this window"
msgstr "এই উইন্ডো বন্ধ কৰক"

#: ../src/gconf-editor-window.c:757
msgid "_Quit"
msgstr "প্ৰস্থান কৰক (_Q)"

#: ../src/gconf-editor-window.c:757
msgid "Quit the Configuration Editor"
msgstr "সংৰূপ সম্পাদক প্ৰস্থান কৰক"

#: ../src/gconf-editor-window.c:760
msgid "_Copy Key Name"
msgstr "চাবিৰ নাম কপি কৰক (_C)"

#: ../src/gconf-editor-window.c:760
msgid "Copy the name of the selected key"
msgstr "নিৰ্বাচিত চাবিৰ নাম কপি কৰক"

#: ../src/gconf-editor-window.c:762
msgid "_Find..."
msgstr "সন্ধান কৰক... (_F)"

#: ../src/gconf-editor-window.c:762
msgid "Find patterns in keys and values"
msgstr "চাবি আৰু মানসমূহত বিন্যাসসমূহ সন্ধান কৰক"

#: ../src/gconf-editor-window.c:764
msgid "_List Recent Keys"
msgstr "নতুন চাবিসমূহক তালিকাভুক্ত কৰক (_L)"

#: ../src/gconf-editor-window.c:764
msgid "Show recently modified keys"
msgstr "সেহতীয়াভাৱে সলনি কৰা চাবিসমূহক দেখুৱাওক"

#: ../src/gconf-editor-window.c:767
msgid "_Add Bookmark"
msgstr "পত্ৰচিহ্ন যোগ কৰক (_A)"

#: ../src/gconf-editor-window.c:767
msgid "Add a bookmark to the selected directory"
msgstr "নিৰ্বাচিত ডাইৰেকটৰিলে এটা পত্ৰচিহ্ন যোগ কৰক"

#: ../src/gconf-editor-window.c:769
msgid "_Edit Bookmarks"
msgstr "পত্ৰচিহ্নসমূহ সম্পাদনা কৰক (_E)"

#: ../src/gconf-editor-window.c:769
msgid "Edit the bookmarks"
msgstr "পত্ৰচিহ্নসমূহ সম্পাদনা কৰক"

#: ../src/gconf-editor-window.c:772
msgid "_Contents"
msgstr "সমলসমূহ (_C)"

#: ../src/gconf-editor-window.c:772
msgid "Open the help contents for the Configuration Editor"
msgstr "সংৰূপ সম্পাদকৰ বাবে সহায় সমলসমূহ খোলক"

#: ../src/gconf-editor-window.c:774
msgid "_About"
msgstr "বিষয়ে (_A)"

#: ../src/gconf-editor-window.c:774
msgid "Show the about dialog for the Configuration Editor"
msgstr "সংৰূপ সম্পাদকৰ বাবে বিষয় ডাইলগ দেখুৱাওক"

#: ../src/gconf-editor-window.c:777
msgid "_New Key..."
msgstr "নতুন চাবি... (_N)"

#: ../src/gconf-editor-window.c:777
msgid "Create a new key"
msgstr "এটা নতুন চাবি সৃষ্টি কৰক"

#: ../src/gconf-editor-window.c:779
msgid "_Edit Key..."
msgstr "চাবি সম্পাদনা কৰক (_E)..."

#: ../src/gconf-editor-window.c:779
msgid "Edit the selected key"
msgstr "নিৰ্বাচিত চাবি সম্পাদনা কৰক"

#: ../src/gconf-editor-window.c:781
msgid "_Unset Key"
msgstr "চাবি অসংহতি কৰক (_U)"

#: ../src/gconf-editor-window.c:781
msgid "Unset the selected key"
msgstr "নিৰ্বাচিত চাবিক অসংহতি কৰক"

#: ../src/gconf-editor-window.c:783
msgid "Set as _Default"
msgstr "অবিকল্পিত হিচাপে সংহতি কৰক (_D)"

#: ../src/gconf-editor-window.c:783
msgid "Set the selected key to be the default"
msgstr "নিৰ্বাচিত চাবি অবিকল্পিত হিচাপে সংহতি কৰক"

#: ../src/gconf-editor-window.c:785
msgid "Set as _Mandatory"
msgstr "বাধ্যতামূলক হিচাপে সংহতি কৰক (_M)"

#: ../src/gconf-editor-window.c:785
msgid "Set the selected key to the mandatory"
msgstr "নিৰ্বাচিত চাবি বাধ্যতামূলক হিচাপে সংহতি কৰক"

#: ../src/gconf-editor-window.c:867
msgid "Configuration Editor (Default settings)"
msgstr "সংৰূপ সম্পাদক (অবিকল্পিত সংহতিসমূহ)"

#: ../src/gconf-editor-window.c:870 ../src/gconf-editor-window.c:1369
msgid "Configuration Editor (Mandatory settings)"
msgstr "সংৰূপ সম্পাদক (বাধ্যতামূলক সংহতিসমূহ)"

#: ../src/gconf-editor-window.c:1115 ../src/gconf-editor-window.c:1116
#: ../src/gconf-editor-window.c:1117 ../src/gconf-editor-window.c:1118
#: ../src/gconf-editor-window.c:1147 ../src/gconf-editor-window.c:1155
#: ../src/gconf-editor-window.c:1164 ../src/gconf-editor-window.c:1715
#: ../src/gconf-editor-window.c:1727 ../src/gconf-editor-window.c:1739
#: ../src/gconf-editor-window.c:1754
msgid "(None)"
msgstr "(কোনো নহয়)"

#: ../src/gconf-editor-window.c:1328 ../src/gconf-editor-window.c:1338
#, c-format
msgid ""
"Cannot create GConf engine. Error was:\n"
"%s"
msgstr ""
"GConf ইঞ্জিন সৃষ্টি কৰা সম্ভৱ নহয় । ত্ৰুটি আছিল:\n"
"%s"

#: ../src/gconf-editor-window.c:1366
msgid "Configuration Editor (Defaults settings)"
msgstr "সংৰূপ সম্পাদক (অবিকল্পিত সংহতিসমূহ)"

#: ../src/gconf-editor-window.c:1611
msgid "Name"
msgstr "নাম"

#: ../src/gconf-editor-window.c:1635
msgid "Value"
msgstr "মান"

#: ../src/gconf-editor-window.c:1662
msgid "Key Documentation"
msgstr "চাবিৰ তথ্যচিত্ৰ"

#: ../src/gconf-editor-window.c:1691 ../src/gconf-key-editor.c:614
msgid "This key is not writable"
msgstr "এই চাবিৰ মান লিখিব নোৱাৰি"

#: ../src/gconf-editor-window.c:1704
msgid "This key has no schema"
msgstr "এই চাবিৰ কোনো আঁচনি নাই"

#: ../src/gconf-editor-window.c:1709
msgid "Key name:"
msgstr "চাবিৰ নাম:"

#: ../src/gconf-editor-window.c:1722
msgid "Key owner:"
msgstr "চাবিৰ গৰাকী:"

#: ../src/gconf-editor-window.c:1734
msgid "Short description:"
msgstr "সংক্ষিপ্ত বিৱৰণ:"

#: ../src/gconf-editor-window.c:1747
msgid "Long description:"
msgstr "বিস্তাৰিত বিৱৰণ:"

#: ../src/gconf-key-editor.c:87
msgid "T_rue"
msgstr "সঁচা (_r)"

#: ../src/gconf-key-editor.c:90 ../src/gconf-key-editor.c:247
#: ../src/gconf-key-editor.c:355 ../src/gconf-key-editor.c:645
msgid "_False"
msgstr "মিছা (_F)"

#. These have to be ordered so the EDIT_ enum matches the
#. * menu indices
#.
#. These have to be ordered so the EDIT_ enum matches the
#. * combobox indices
#.
#: ../src/gconf-key-editor.c:125 ../src/gconf-key-editor.c:155
msgid "Integer"
msgstr "পূৰ্ণ সংখ্যা"

#: ../src/gconf-key-editor.c:126 ../src/gconf-key-editor.c:156
msgid "Boolean"
msgstr "বুলিয়েন"

#: ../src/gconf-key-editor.c:127 ../src/gconf-key-editor.c:157
msgid "String"
msgstr "স্ট্ৰিং"

#. Translators: this refers to "Floating point":
#. * see http://en.wikipedia.org/wiki/Floating_point
#.
#: ../src/gconf-key-editor.c:131
msgid "Float"
msgstr "দশমিক"

#: ../src/gconf-key-editor.c:132
msgid "List"
msgstr "তালিকা"

#: ../src/gconf-key-editor.c:220
msgid "Add New List Entry"
msgstr "নতুন তালিকা প্ৰবিষ্টি যোগ কৰক"

#: ../src/gconf-key-editor.c:238
msgid "_New list value:"
msgstr "নতুন তালিকাৰ মান (_N):"

#: ../src/gconf-key-editor.c:325
msgid "Edit List Entry"
msgstr "তালিকাৰ প্ৰবিষ্টি সম্পাদনা কৰক"

#: ../src/gconf-key-editor.c:343
msgid "_Edit list value:"
msgstr "তালিকাৰ মান সম্পাদনা কৰক (_E):"

#: ../src/gconf-key-editor.c:566
msgid "Path:"
msgstr "পথ:"

#: ../src/gconf-key-editor.c:577
msgid "_Name:"
msgstr "নাম (_N):"

#: ../src/gconf-key-editor.c:589
msgid "_Type:"
msgstr "ধৰণ (_T):"

#: ../src/gconf-key-editor.c:626 ../src/gconf-key-editor.c:644
#: ../src/gconf-key-editor.c:663 ../src/gconf-key-editor.c:680
msgid "_Value:"
msgstr "মান (_V):"

#: ../src/gconf-key-editor.c:702
msgid "List _type:"
msgstr "তালিকাৰ ধৰণ (_t):"

#: ../src/gconf-key-editor.c:710
msgid "_Values:"
msgstr "মানসমূহ (_V):"

#: ../src/gconf-key-editor.c:812
msgid "New Key"
msgstr "নতুন চাবি"

#: ../src/gconf-key-editor.c:816
msgid "Edit Key"
msgstr "চাবি সম্পাদনা কৰক"

#: ../src/gconf-search-dialog.c:60
msgid "Pattern not found"
msgstr "বিন্যাস পোৱা নগল"

#: ../src/gconf-search-dialog.c:142
msgid "Find"
msgstr "সন্ধান কৰক"

#: ../src/gconf-search-dialog.c:144
msgid "_Search for: "
msgstr "-ৰ বাবে সন্ধান কৰক (_S): "

#: ../src/gconf-search-dialog.c:165
msgid "Search also in key _names"
msgstr "চাবিৰ নামতো সন্ধান কৰক (_n)"

#: ../src/gconf-search-dialog.c:168
msgid "Search also in key _values"
msgstr "চাবিৰ মানতো সন্ধান কৰক (_v)"

#: ../src/gedit-output-window.c:347
msgid "Close the output window"
msgstr "আউটপুট উইন্ডো বন্ধ কৰক"

#: ../src/gedit-output-window.c:382
msgid "Copy selected lines"
msgstr "নিৰ্বাচিত শাৰীসমূহ কপি কৰক"

#: ../src/gedit-output-window.c:399
msgid "Clear the output window"
msgstr "আউটপুটৰ উইন্ডো পৰিষ্কাৰ কৰক"

#: ../src/gedit-output-window.c:438
msgid "Output Lines"
msgstr "আউটপুট শাৰীসমূহ"

#: ../src/main.c:69
msgid "[KEY]"
msgstr "[KEY]"

#: ../src/main.c:77
msgid "- Directly edit your entire configuration database"
msgstr "- আপোনাৰ সম্পূৰ্ণ সংৰূপ ডাটাবেইচ প্ৰত্যক্ষভাৱে সম্পাদন কৰক"

#~ msgid "Type"
#~ msgstr "ধৰন"

#~ msgid "The Configuration Editor window type."
#~ msgstr "সংৰূপ সম্পাদক উইন্ডোৰ ধৰণ ।"
